package de.weloveapps.videoplayersample;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.VideoView;

public class MainActivity extends AppCompatActivity {

    private static final String VIDEO_PATH = "http://hier-gehts-app.de/extern/video/bbb_1080p_30fps_0.mp4";

    private VideoView mVideoView1;
    private VideoView mVideoView2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mVideoView1 = (VideoView) findViewById(R.id.video1);
        mVideoView2 = (VideoView) findViewById(R.id.video2);

        mVideoView1.setVideoPath(VIDEO_PATH);
        mVideoView2.setVideoPath(VIDEO_PATH);
    }

    @Override
    protected void onStart() {
        super.onStart();

        mVideoView1.start();
        mVideoView2.start();
    }

    @Override
    protected void onStop() {
        super.onStop();

        mVideoView1.stopPlayback();
        mVideoView2.stopPlayback();
    }
}
